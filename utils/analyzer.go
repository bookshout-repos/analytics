package utils

import (
	"strings"
)

//Analyzer does the heavy lifting of running through the file and producing desired output
func Analyzer (fileName string)(numSellableUS int, numNonSellableUS int, avgPrice float64 ){
	
	//setup inter-process communications
	data := make(chan *BookRecord)
	done := make(chan bool)
	defer close(data)
	defer close(done)
	numSellableUS = 0
	numNonSellableUS = 0
	avgPrice = 0.0
	totalPrice := 0
	numBooks := 0
	go streamRecords(fileName, data, done)
	Loop:
		for {
			select {
			case <- done:
				break Loop
			case d := <-data:
				numBooks++
				if isSellableUS(d) {
					numSellableUS++
				} else {
					numNonSellableUS++
				}
				totalPrice += d.Price
			default:
			}

		}
	if numBooks != 0 {
		avgPrice = float64(totalPrice/ numBooks)/100.0
	}
	return
}

func isSellableUS(rec *BookRecord) bool {
	
	//Non-sellable indicators supercede
	for _, a := range rec.NonSalesRights{
		if strings.EqualFold(a, "US") || strings.EqualFold(a, "WORLD") {
			return false
		}
	}

	for _, a := range rec.SalesRights{
		if strings.EqualFold(a, "US") || strings.EqualFold(a, "WORLD") {
			return true
		}
	}
	return false
}

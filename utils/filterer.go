package utils

import (
	"strings"
	"fmt"
)

//Filterer does the heavy lifting of running through the file and producing desired output
func Filterer (fileName string, category string){
	
	//setup inter-process communications
	data := make(chan *BookRecord)
	done := make(chan bool)
	defer close(data)
	defer close(done)
	go streamRecords(fileName, data, done)
	Loop:
		for {
			select {
			case <- done:
				break Loop
			case d := <-data:
				if categoryMatches(d, category) {
					fmt.Printf("%v, %v, $%0.2f\n", d.ISBN, d.Title, float64(d.Price)/100.0)
				}
			default:
			}

		}
}

func categoryMatches(rec *BookRecord, cat string) bool {
	for _, a := range rec.Categories{
		if strings.EqualFold(a, cat) {
			return true
		}
	}
	return false
}
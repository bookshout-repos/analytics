package utils

import (
//	"gopkg.in/go-playground/validator.v9"
)

//BookRecord represents one row of data for a book split into respective fields
type BookRecord struct {
	ISBN string				`validate:"required"`
	Title string			`validate:"required"`
	Description string		`validate:"required"`
	Categories []string		`validate:"required"`
	SalesRights []string	`validate:"required"`
	NonSalesRights []string	`validate:"required"`
	Price int				`validate:"required"`
}
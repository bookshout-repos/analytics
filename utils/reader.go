package utils

import (
	"strconv"
	"io"
	"os"
	"strings"

	"encoding/csv"
)
//streamRecords does the following:
//1. Read input file data row by row into a  BookRecord struct
//2. Validate fields and discard the whole row if any one field is invalid --> done automatically by BookRecord's validate tag
//3. Provide BookRecord struct to the channel that connects it to the calling routine
//4. Signal on done channel once all rows in input data file are read
func streamRecords(fileName string, dataCh chan *BookRecord, doneCh chan bool) error {
	dataFile, err := os.Open(fileName)
	if err != nil {
		doneCh <- true
		return err
	}
	defer dataFile.Close()

	reader := csv.NewReader(dataFile)
	reader.Comma = '|'
	reader.FieldsPerRecord = 0 //reader will guess fields per record by looking at the header row
	i := 0
	for {
		rec, err := reader.Read()

		if err == io.EOF {
			doneCh <- true
			break
		}

		if err == nil  {
			
			//ignore header row
			if i == 0 {
				i++
				continue
			}
			
			//ignore row that produces error
			price, err := strconv.Atoi(rec[6])
			if err != nil {
				continue
			}
			newRec := BookRecord{
				ISBN: rec[0],
				Title: rec[1],
				Description: rec[2],
				Categories: strings.Split(rec[3], ","),
				SalesRights: strings.Split(rec[4], ","),
				NonSalesRights: strings.Split(rec[5],","),
				Price: price,
			}

			// if newRec could get instantiated successfully, that means all validations passed
			dataCh<- &newRec
		}
		//ignore rows that produce an error, so nothing to do if err != nil
		i++

	}
	return nil
}
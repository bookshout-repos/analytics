package main

import (
	"os"

	"gitlab.com/bookshout/analytics/cmd"
)

func main() {
	defer os.Exit(0)
	cmd.Execute()
}
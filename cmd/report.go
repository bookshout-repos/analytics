package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/bookshout/analytics/utils"
)

var reportCmd = &cobra.Command{
	Use: "report <file-name>",
	Short: "returns all data rows in <file-name> that represent books that are sellable in US ",
	Long: `report command returns all records in <file-name> that represent books that are sellable in US.
<file-name> must be a valid pipe separated text file with the following structure:
			ISBN|TITLE|DESCRIPTION|CATEGORIES|SALES RIGHTS|NON-SALES RIGHTS|PRICE`,
	RunE: runReport,
}

func init() {
	rootCmd.AddCommand(reportCmd)
}
func runReport(c *cobra.Command, args []string) error {
	if 	err := cobra.ExactArgs(1)(c, args); err != nil {
		return err
	}
	utils.Reporter(args[0])
	return nil
}

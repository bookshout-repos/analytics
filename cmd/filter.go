package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/bookshout/analytics/utils"
)

var filterCmd = &cobra.Command{
	Use: "filter <file-name> <category>",
	Short: "returns all the data rows in  <file-name> that match the <category>",
	Long: `filter command returns all the data rows in  <file-name> that match the <category>. <file-name> must be a valid pipe separated text file with the following structure:
			ISBN|TITLE|DESCRIPTION|CATEGORIES|SALES RIGHTS|NON-SALES RIGHTS|PRICE`,
	RunE: runFilter,
}

func init() {
	rootCmd.AddCommand(filterCmd)
}
func runFilter(c *cobra.Command, args []string) error {
	if 	err := cobra.ExactArgs(2)(c, args); err != nil {
		return err
	}
	utils.Filterer(args[0], args[1])
	return nil
}

package cmd

import (
	"os"
	 "fmt"

	"github.com/spf13/cobra"
)


var rootCmd = &cobra.Command{
	Use: "bookshout-analytics <command> <args>",
	Short: "command line analytics utility",
	Long: `bookshout-analytics is a command line utility that provides reporting, analysis and filtering of text based book data.`,
}

//Execute is the main function that runs this command line application
func Execute(){
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

	package cmd

	import (
		"fmt"
		"github.com/spf13/cobra"
		"gitlab.com/bookshout/analytics/utils"
	)

	var analyzeCmd = &cobra.Command{
		Use: "analyze <file-name>",
		Short: "returns specific analytics about book data in <file-name>",
		Long: `analyze command returns specific analytics about book data in <file-name>. <file-name> must be a valid pipe separated text file with the following structure:
				ISBN|TITLE|DESCRIPTION|CATEGORIES|SALES RIGHTS|NON-SALES RIGHTS|PRICE
				
	Analytics returned by this command are:
		number of sellable books in US
		number of non-sellable books in US
		The average price across all books in the file`,
		RunE: runAnalyze,
	}

	func init() {
		rootCmd.AddCommand(analyzeCmd)
	}
	func runAnalyze(c *cobra.Command, args []string) error {
		if 	err := cobra.ExactArgs(1)(c, args); err != nil {
			return err
		}
		numSellable, numNonSellable, avgPrice := utils.Analyzer(args[0])
		fmt.Println("Summary:")
		fmt.Printf("Sellable in US:\t\t%d\n", numSellable)
		fmt.Printf("Not Sellable in US:\t%d\n", numNonSellable)
		fmt.Printf("Average Price:\t\t$%0.2f\n", avgPrice)
		return nil
	}

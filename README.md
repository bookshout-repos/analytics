# Introduction
`bookshout-analytics` is a command line utility that provide analytics, category-based filtering and reports on books eligible to be sold in the US.

# Installation
`bookshout-analytics` may be installed in one of two ways:

1.  Copy the executable `bookshout-analytics` to a location within the system path. This executable is compiled for 64-bit Windows only.
2. Build from sources:

    (a) Install [GO](https://golang.org/doc/install) on your machine

    (b) clone the gitlab directory into your $GOPATH/:

    ```
    git clone git@gitlab.com:bookshout-repos/analytics.git
    ```
    Note that the clone must happen in `go/src/gitlab.com/bookshout/analytics` and not in `go/src/gitlab.com/bookshout-repos/analytics` so you may have to run a `git remote set-url` command before cloning
    (c) Build `bookshout-analytics` from sources (details below pertain to 64-bit windows; you may need to make adjustments as appropriate)

    First,install all 64-bit Windows specific go packages so the build would be faster
    ```
    GOOS=windows GOARCH=amd64 go install
    ```

    Next, build the cloned repo:
    ```
    go build -o bookshout-analytics main.go
    ```
    If everything went well, typing `bookshout-analytics` on the command line should give you the standard usage message:
    ```
    $ bookshout-analytics
        bookshout-analytics is a command line utility that provides reporting, analysis and filtering of text based book data.

        Usage:
        bookshout-analytics [command]

        Available Commands:
        analyze     returns specific analytics about book data in <file-name>
        filter      returns all the data rows in  <file-name> that match the <category>
        help        Help about any command
        report      returns all data rows in <file-name> that represent books that are sellable in US

        Flags:
        -h, --help   help for bookshout-analytics

        Use "bookshout-analytics [command] --help" for more information about a command.
    ```

    (d) Explore command line options by typing
    ```
    $ bookshout-analytics <command> --help
    ```
# Sample Output

Assuming you have a well-formed input data file called [input.txt](https://gist.githubusercontent.com/rjregenold/8bd00e574614988704323b3b41d423b6/raw/001b4d494eaee4ab359d45534365221d8db98041/input.txt). Here is what to expect by way out output:

```
$ bookshout-analytics analyze input.txt
Summary:
Sellable in US:         3
Not Sellable in US:     2
Average Price:          $10.99
```

```
$ bookshout-analytics report input.txt
0000000000001, Book 1, $7.99
0000000000002, Book 2, $29.99
0000000000005, Book 5, $0.99
```

```
$ bookshout-analytics filter input.txt "non fiction"
0000000000002, Book 2, $29.99
0000000000004, Book 4, $1.99
```

Enjoy!!